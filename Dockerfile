FROM ubuntu:22.04

RUN apt update -y && apt install -y dnsutils inetutils-ping openssh-client

RUN mkdir -p /root/.ssh/ && \
    touch ~/.ssh/known_hosts && \
    chmod 700 ~/.ssh && \
    chmod 600 ~/.ssh/known_hosts && \
    ssh-keyscan -H www.365up.cloud >> ~/.ssh/known_hosts
